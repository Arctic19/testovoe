import React from "react";
import ReactDOM from "react-dom";
import styled from "styled-components";
import App from "./App";

const rootElement = document.getElementById("root");
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  rootElement
);
